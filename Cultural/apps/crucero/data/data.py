from apps.crucero.models import Cliente,Actividad,Artista,Asignacion,Categoria,Escenario,Reserva

#####	 Datos para Cliente 	#########

## 1er Paquete	##
p = Cliente(
		#id=1,
		cedula=111,
		nombre='Yerson Ferney Porras',
		edad=25,
		telefono='555-555',
		serial=10001
)
p.save()

p = Cliente(
		#id=2,
		cedula=112,
		nombre='Jessica Faisuri López',
		edad=21,
		telefono='555-555',
		serial=10001
)
p.save()


## 2do Paquete	##
p = Cliente(
		#id=3,
		cedula=211,
		nombre='Hernán Oswaldo Porras',
		edad=18,
		telefono='555-555',
		serial=10002
)
p.save()

p = Cliente(
		#id=4,
		cedula=212,
		nombre='Edgar Porras',
		edad=50,
		telefono='555-555',
		serial=10002
)
p.save()

p = Cliente(
		#id=5,
		cedula=213,
		nombre='Juan José Porras',
		edad=10,
		telefono='555-555',
		serial=10002
)
p.save()


## 3er Paquete	##
p = Cliente(
		#id=6,
		cedula=311,
		nombre='Jorge Alejandro Romero',
		edad=20,
		telefono='555-555',
		serial=10003
)
p.save()

p = Cliente(
		#id=7,
		cedula=312,
		nombre='Gabriel Romero',
		edad=24,
		telefono='555-555',
		serial=10003
)
p.save()

p = Cliente(
		#id=8,
		cedula=313,
		nombre='Jorge Romero',
		edad=59,
		telefono='555-555',
		serial=10003
)
p.save()

p = Cliente(
		#id=9,
		cedula=314,
		nombre='Juan David Romero',
		edad=13,
		telefono='555-555',
		serial=10003
)
p.save()

p = Cliente(
		#id=10,
		cedula=315,
		nombre='Mariana Romero',
		edad=16,
		telefono='555-555',
		serial=10003
)
p.save()

p = Cliente(
		#id=11,
		cedula=316,
		nombre='Doris Calderón',
		edad=46,
		telefono='555-555',
		serial=10003
)
p.save()



#####	 Datos para Artista 	#########
p = Artista(
		#id=1,
		nombre='Juanes',
		especialidad='Cantante de música pop',
		estado='SI'
)
p.save()

p = Artista(
		#id=2,
		nombre='John Cena',
		especialidad='Exculturista, actor, cantante rapero y luchador profesional estadounidense que trabaja para la WWE',
		estado='SI'
)
p.save()

p = Artista(
		#id=3,
		nombre='Sasha Grey',
		especialidad='Exactriz pornográfica, modelo, escritora, streamer, gamer y actriz estadounidense',
		estado='NO'
)
p.save()

p = Artista(
		#id=4,
		nombre='Mickey Mouse',
		especialidad='Personaje ficticio de la serie del mismo nombre, emblema de la compañía Disney',
		estado='SI'
)
p.save()

p = Artista(
		#id=5,
		nombre='Scarlett Johansson',
		especialidad='Actriz de cine, cantante y modelo estadounidense',
		estado='SI'
)
p.save()


#####	 Datos para Categoria 	#########
p = Categoria(
		#id=1,
		nombre='Todo Público',
		descripcion='Actividad destinada para personas de cualquier rango de edad'
)
p.save()

p = Categoria(
		#id=2,
		nombre='Infantil',
		descripcion='Actividad destinada para público infantil (<14 años)'
)
p.save()

p = Categoria(
		#id=3,
		nombre='Jóven',
		descripcion='Actividad destinada para público joven en adelante(>14 años)'
)
p.save()

p = Categoria(
		#id=4,
		nombre='Adultos',
		descripcion='Actividad destinada para público adulto (>18 años)'
)
p.save()


#####	 Datos para Edad 	#########
p = Edad(
		#id=1,
		nombre=0
)
p.save()

p = Edad(
		#id=1,
		nombre=1
)
p.save()

p = Edad(
		#id=1,
		nombre=14
)
p.save()

p = Edad(
		#id=1,
		nombre=18
)
p.save()


#####	 Datos para Escenario 	#########
p = Escenario(
		#id=1,
		nombre='Piscina',
		capacidad=10
)
p.save()

p = Escenario(
		#id=2,
		nombre='Discoteca',
		capacidad=8
)
p.save()

p = Escenario(
		#id=3,
		nombre='Centro',
		capacidad=9
)
p.save()

p = Escenario(
		#id=4,
		nombre='Bar',
		capacidad=8
)
p.save()

p = Escenario(
		#id=5,
		nombre='Casino',
		capacidad=11
)
p.save()

p = Escenario(
		#id=6,
		nombre='Gimnasio',
		capacidad=8
)
p.save()

p = Escenario(
		#id=7,
		nombre='Restaurante',
		capacidad=9
)
p.save()

p = Escenario(
		#id=8,
		nombre='Centro Comercial',
		capacidad=8
)
p.save()

p = Escenario(
		#id=9,
		nombre='Centro Recreacional Infantil',
		capacidad=8
)
p.save()

p = Escenario(
		#id=10,
		nombre='Centro Recreacional',
		capacidad=8
)
p.save()


#ERROR CON LAS KEY ID
"""
#####	 Datos para Actividad 	#########
p = Actividad(
		id=1,
		nombre='Concierto Juanes más cerca!',
		fecha_inicio="2019-05-29 19:00:00-05",
		fecha_fin="2019-05-29 23:00:00-05",
		limite_edad=0,
		id_categoria=3
)
p.save()

p = Actividad(
		id=1,
		nombre='Ven y conoce a Mickey Mouse!',
		fecha_inicio="2019-05-30 13:00:00-05",
		fecha_fin="2019-05-30 19:00:00-05",
		limite_edad=0,
		id_categoria=2
)
p.save()


#####	 Datos para Asignacion 	#########
p = Asignacion(
		id=1,
		cupo=6,
		id_escenario=2,
		id_artista=1,
		id_actividad=1
)
p.save()

p = Asignacion(
		id=2,
		cupo=4,
		id_escenario=9,
		id_artista=4,
		id_actividad=2
)
p.save()


#####	 Datos para Reserva 	#########
p = Reserva(
		id=1,
		cantidad_res=2,
		id_cliente=3,
		id_asignacion=1
)
p.save()

p = Reserva(
		id=2,
		cantidad_res=4,
		id_cliente=11,
		id_asignacion=2
)
p.save()
"""