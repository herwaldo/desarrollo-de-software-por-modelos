# -*- coding: utf-8 -*-
from django.db import models


SIONO = (
    ('1', 'Habilitado'),
    ('2', 'Deshabilitado'),
)

ESTADO = (
    ('1', 'Activa'),
    ('2', 'Cancelada'),
)


class Cliente(models.Model):
    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
    cedula = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    edad =  models.IntegerField(default=0)
    telefono = models.CharField(max_length=50)
    serial = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre


class Artista(models.Model):
    class Meta:
        verbose_name = 'Artista'
        verbose_name_plural = 'Artistas'
    nombre = models.CharField(max_length=30)
    especialidad = models.CharField(max_length=1000)
    estado = models.CharField(max_length=2, choices=SIONO, verbose_name='Estado')

    def __str__(self):
        return self.nombre


class Categoria(models.Model):
    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
    nombre = models.CharField(max_length=100)
    descripcion =  models.CharField(max_length=3000)

    def __unicode__(self):
        return unicode(self.nombre)


class Edad(models.Model):
    class Meta:
        verbose_name = 'Edad'
        verbose_name_plural = 'Edad'
    nombre = models.PositiveIntegerField(verbose_name='Edad')

    def __str__(self):
        return str(self.nombre)


class Escenario(models.Model):
    class Meta:
        verbose_name = 'Escenario'
        verbose_name_plural = 'Escenarios'
    nombre = models.CharField(max_length=500)
    capacidad = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre



class Actividad(models.Model):
    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'
    nombre = models.CharField(max_length=500)
    fecha_inicio =  models.DateTimeField('Hora y Fecha de Inicio')
    fecha_fin = models.DateTimeField('Hora y Fecha de Finalización')
    limite_edad = models.ForeignKey(Edad, verbose_name='Limite de edad')
    id_categoria = models.ForeignKey(Categoria, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class Asignacion(models.Model):
    class Meta:
        verbose_name = 'Asignacion'
        verbose_name_plural = 'Asignaciones'
    cupo = models.IntegerField(default=0)
    cupo_reservado = models.IntegerField(default=0)
    id_escenario = models.ForeignKey(Escenario, null=False, blank=False, on_delete=models.CASCADE)
    id_artista =  models.ForeignKey(Artista, null=False, blank=False, on_delete=models.CASCADE)
    id_actividad = models.ForeignKey(Actividad, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)


class Reserva(models.Model):
    class Meta:
        verbose_name = 'Reserva'
        verbose_name_plural = 'Reservas'
    cantidad_res = models.IntegerField(default=0)
    id_cliente =  models.ForeignKey(Cliente, null=False, blank=False, on_delete=models.CASCADE)
    id_asignacion = models.ForeignKey(Asignacion, null=False, blank=False, on_delete=models.CASCADE)
    estado = models.CharField(max_length=2, choices=ESTADO, verbose_name='Estado')

    def __str__(self):
        return str(self.cantidad_res)