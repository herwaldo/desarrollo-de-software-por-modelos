# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-31 19:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crucero', '0011_asignacion_estado'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='asignacion',
            name='estado',
        ),
        migrations.AddField(
            model_name='reserva',
            name='estado',
            field=models.CharField(choices=[(b'1', b'Activa'), (b'2', b'Cancelada')], default=1, max_length=2, verbose_name=b'Estado'),
            preserve_default=False,
        ),
    ]
