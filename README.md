# - Desarrollo de Software dirigido por modelos - 
# CRUCERO | Módulo Actividad Cultural

## Descripción del dominio.
Proveer actividades culturales a los diferentes visitantes del crucero, clasificando nuestros eventos según su género y las edades de los participantes para el mayor disfrute de nuestros clientes. Dichas actividades serán realizadas por diferentes artistas según su modalidad. Con el fin de tener organización en todas las actividades se deberá hacer las reservas para evitar aglomeraciones.

## Descripción general del alcance.
El sistema de gestión de actividades culturales incorpora las funcionalidades de gestión de actividades culturales, gestión de artistas, clasificación de actividades y la realización de reservas. 

## Breve descripción de los módulos / submódulos.
- **Gestionar artista:** Permite gestionar los artistas para las actividades culturales
- **Agregar Artista:** crear, actualizar y visualizar los artistas
- **Inhabilitar artista:** permite inhabilitar artistas que ya no trabajan con el crucero.
- **Actividad:** permite desarrollar todas las operaciones necesarias para que la actividad pueda ser visualizada, seleccionada o clasificada según sus atributos.
- **Clasificar actividad:** Permite categorizar las actividades por el tipo de actividad y por las edades de los participantes
- **Listar actividad:** visualiza todas las actividades que se pueden desarrollar en el crucero
- **Gestionar actividad:** Asigna el artista que va a desarrollar la actividad y el lugar donde se va a realizar. 
- **Crear actividad:** crea, actualiza y visualiza las actividades
- **Eliminar actividad:** permite eliminar las actividades ya realizadas o que no haya sido reservada por un cliente. 
- **Reserva:** permite gestionar las reservas a las actividades culturales
- **Crear reserva:** permite al cliente realizar la reserva para él y sus acompañantes a una actividad y lleva el control de la capacidad de asistencia a cada actividad.
- **Cancelar reserva:** Permite cancelar una reserva antes del evento al cual se inscribió el cliente .

## Descripción del Lenguaje de Programación.
Python es un lenguaje de programación de fácil aprendizaje. Cuenta con estructuras de datos eficientes y de alto nivel; mantiene un enfoque simple pero efectivo a la programación orientada a objetos. La elegante sintaxis de Python y su tipado dinámico, junto con su naturaleza interpretada, hacen de éste un lenguaje ideal para scripting y desarrollo rápido de aplicaciones en diversas áreas y sobre la mayoría de las plataformas. El intérprete de Python y la extensa biblioteca estándar están a libre disposición en forma binaria y de código fuente para las principales plataformas desde el sitio web de Python, https://www.python.org/, y puede distribuirse libremente.
El intérprete de Python puede extenderse fácilmente con nuevas funcionalidades y tipos de datos implementados en C o C++ (u otros lenguajes accesibles desde C). Python también puede usarse como un lenguaje de extensiones para aplicaciones personalizables.

## Descripción del Framework.
Django es un framework de aplicaciones web gratuito y de código abierto (open source) escrito en Python. Siendo un framework orientado a la web con un conjunto de componentes que ayudan a desarrollar sitios web más fácil y rápidamente, el framework respeta el patrón de diseño Modelo - Vista - Template.
### Característica de Django:
- Un mapeador objeto-relacional.
- Aplicaciones "enchufables" que pueden instalarse en cualquier página gestionada con Django.
- Una API de base de datos robusta.
- Un sistema incorporado de "vistas genéricas" que ahorra tener que escribir la lógica de ciertas tareas comunes.
- Un sistema extensible de plantillas basado en etiquetas, con herencia de plantillas.
- Un despachador de URLs basado en expresiones regulares.
- Un sistema "*middleware*" para desarrollar características adicionales; por ejemplo, la distribución principal de Django incluye componentes middleware que proporcionan cacheo, compresión de la salida, normalización de URLs, protección CSRF y soporte de sesiones.
- Soporte de internacionalización, incluyendo traducciones incorporadas de la interfaz de administración.
- Documentación incorporada accesible a través de la aplicación administrativa (incluyendo documentación generada automáticamente de los modelos y las bibliotecas de plantillas añadidas por las aplicaciones).

## Modelo Entidad Relación:
![](https://gitlab.com/herwaldo/desarrollo-de-software-por-modelos/raw/master/Cultural/Images/ER_DB.png)


## Participantes:
- Yerson Ferney Porras
- Yefersson Adrian Rojas
- Olga Lucia Balaguera
- Hernán Oswaldo Porras

