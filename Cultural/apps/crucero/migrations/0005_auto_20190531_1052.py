# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-31 15:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crucero', '0004_auto_20190530_1706'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actividad',
            name='fecha_fin',
            field=models.DateTimeField(verbose_name=b'Hora y Fecha de Finalizaci\xc3\xb3n'),
        ),
    ]
