# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from easy_pdf.views import PDFTemplateView
from django.conf import settings
import datetime as dt

# Create your views here.


def index(request):
    return HttpResponse('Index')


def agregar_artista(request):
    return HttpResponse('esta es la vista para gestiòn de artista')


def ver_artistas(request):
    return render_to_response(
        'verArtistas.html',
        {'artistas': artistas}
    )


def inhabilitar_artista(request, id):
    print(id)
    return render_to_response(
        'inhabilitarArtista.html',
        {'artistas': artistas}
    )


def clasificar_actividades(request):
    return HttpResponse('esta es la vista para gestiòn de artista')


def listar_actividades(request):
    return HttpResponse('esta es la vista para gestiòn de artista')


def crear_actividad(request):
    return HttpResponse('esta es la vista para gestiòn de artista')


def eliminar_actividades(request):
    return HttpResponse('esta es la vista para gestiòn de artista')


def crer_reserva(request):
    return HttpResponse('esta es la vista para gestiòn de artista')


def cancelar_reserva(request):
    return HttpResponse('esta es la vista para gestiòn de artista')