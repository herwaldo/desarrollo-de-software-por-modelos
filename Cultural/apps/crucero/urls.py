# -*- coding: utf-8 -*-
from django.conf.urls import url,include
from . import views
#from django.contrib import admin

urlpatterns = [
    url(r'^$', views.index),
    url(r'^agregar_artista$', views.agregar_artista),
    url(r'^ver_artistas', views.ver_artistas),
    url(r'^eliminar_artista$/?(?P<id>\d+)?/?$', views.inhabilitar_artista, name='Inhabilitar'),

    url(r'^clasificar_actividad$', views.clasificar_actividades),
    url(r'^listar_actividad$', views.listar_actividades),
    url(r'^crear_actividad$', views.crear_actividad),
    url(r'^eliminar_actividad$', views.eliminar_actividades),

    url(r'^crear_reserva$', views.crer_reserva),
    url(r'^cancelar_reserva$', views.cancelar_reserva),
]

#admin.site.site_header = 'Modulo Actividades Culturales'