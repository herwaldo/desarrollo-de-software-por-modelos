# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Cliente, Actividad, Artista, Asignacion, Categoria, Escenario, Reserva, Edad
from django.contrib import messages


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ('cedula', 'nombre', 'telefono')
    list_display_links = ('cedula', 'nombre', 'telefono')
    search_fields = ('nombre', 'cedula')
    list_per_page = 30

    def save_model(self, request, obj, form, change):
        super(ClienteAdmin, self).save_model(request, obj, form, change)


@admin.register(Actividad)
class ActividadAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'fecha_inicio', 'fecha_fin', 'limite_edad')
    list_display_links = ('nombre', 'fecha_inicio', 'fecha_fin', 'limite_edad')
    search_fields = ('nombre',)
    raw_id_fields = ('id_categoria', 'limite_edad')
    list_filter = ('id_categoria', 'limite_edad' )
    list_per_page = 30

    def save_model(self, request, obj, form, change):
        super(ActividadAdmin, self).save_model(request, obj, form, change)


@admin.register(Artista)
class ArtistaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'especialidad', 'estado')
    list_display_links = ('nombre', 'especialidad', 'estado')
    list_filter = ('estado',)
    search_fields = ('nombre',)
    list_per_page = 30

    def save_model(self, request, obj, form, change):
        super(ArtistaAdmin, self).save_model(request, obj, form, change)


@admin.register(Asignacion)
class AsignacionAdmin(admin.ModelAdmin):
    list_display = ('id_escenario', 'id_artista', 'id_actividad', 'cupo', 'cupo_reservado')
    list_display_links = ('cupo', 'id_escenario', 'id_artista', 'id_actividad', 'cupo_reservado')
    search_fields = ('cupo',)
    raw_id_fields = ('id_escenario', 'id_artista', 'id_actividad')
    exclude = ('cupo_reservado',)
    list_per_page = 30

    def save_model(self, request, obj, form, change):
        super(AsignacionAdmin, self).save_model(request, obj, form, change)


@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion')
    list_display_links = ('nombre', 'descripcion')
    search_fields = ('nombre',)
    list_per_page = 30

    def save_model(self, request, obj, form, change):
        super(CategoriaAdmin, self).save_model(request, obj, form, change)


@admin.register(Escenario)
class EscenarioAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'capacidad')
    list_display_links = ('nombre', 'capacidad')
    search_fields = ('nombre',)
    list_per_page = 30

    def save_model(self, request, obj, form, change):
        super(EscenarioAdmin, self).save_model(request, obj, form, change)


@admin.register(Reserva)
class ReservaAdmin(admin.ModelAdmin):
    list_display = ('cantidad_res', 'id_cliente', 'id_asignacion')
    list_display_links = ('cantidad_res', 'id_cliente', 'id_asignacion')
    search_fields = ('cantidad_res',)
    raw_id_fields = ('id_cliente', 'id_asignacion')
    list_filter = ('estado',)
    list_per_page = 30

    def save_model(self, request, obj, form, change):
        asigna = Asignacion.objects.get(pk=obj.id_asignacion.pk)
        activi = Actividad.objects.get(pk=asigna.id_actividad.pk)
        limiteEdad = activi.limite_edad.nombre

        if (int(asigna.cupo) - int(asigna.cupo_reservado)) >= int(obj.cantidad_res):

            persona = Cliente.objects.get(pk = obj.id_cliente.pk)
            listadoPersonas = Cliente.objects.filter(serial=persona.serial)

            if len(listadoPersonas) < int(obj.cantidad_res):
                messages.set_level(request, messages.ERROR)
                messages.add_message(request, messages.ERROR, 'Los cantidad de cupos que intentan reservar es mayor que la cantidad de personas en el grupo.')
                return
            else:
                contadorEdad = 0
                for p in listadoPersonas:
                    if int(p.edad) < int(limiteEdad):
                        contadorEdad = contadorEdad + 1
                if int(obj.cantidad_res) > len(listadoPersonas) - int(contadorEdad):
                    messages.set_level(request, messages.ERROR)
                    messages.add_message(request, messages.ERROR,'Hay reservas para personas no aptas por rango de edad.')
                    return
                else:
                    asigna.cupo_reservado = int(asigna.cupo_reservado) + int(obj.cantidad_res)
                    asigna.save()
        else:
            messages.set_level(request, messages.ERROR)
            messages.add_message(request, messages.ERROR, 'No hay todos los cupos disponibles.')
            return
        super(ReservaAdmin, self).save_model(request, obj, form, change)


admin.site.register(Edad)